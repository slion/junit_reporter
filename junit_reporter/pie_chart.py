import matplotlib.pyplot as plt


def fractional_pie_label(percentage, all_vals):
    all_vals_sum = sum(all_vals)
    absolute = int(round(percentage / 100.0 * all_vals_sum))
    return "{:d}/{:d}".format(absolute, all_vals_sum)


def save_figure(values, output_path):
    # pie chart, where the slices will be ordered and plotted counter-clockwise:
    colors = {"failed": "#dc3545", "passed": "#28a745", "skipped": "#ffc107"}
    labels, data = zip(*sorted(values.items()))

    fig1, ax1 = plt.subplots(figsize=(2, 2))
    ax1.pie(
        data,
        labels=labels,
        autopct=lambda pct: fractional_pie_label(pct, data),
        shadow=False,
        startangle=90,
        colors=[colors[label] for label in  labels],
    )

    # equal aspect ratio ensures that pie is drawn as a circle.
    ax1.axis("equal")

    plt.savefig(output_path, format="svg")
