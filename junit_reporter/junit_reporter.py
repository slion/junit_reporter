# -*- coding: utf-8 -*-

import os
from pathlib import Path

from jinja2 import Environment, FileSystemLoader
from junitparser import JUnitXml, TestSuite, Skipped

from .pie_chart import save_figure

root_path = Path(__file__).resolve().parent


def read_junit(junit_file_paths):

    for input_path in junit_file_paths:
        xml = JUnitXml.fromfile(input_path)

        for element in xml:
            if isinstance(element, TestSuite):
                # handle suites
                for case in element:
                    # handle cases
                    yield case
            else:
                yield element


def extract_junit_content(junit_file_paths):

    summary = {"passed": 0, "failed": 0, "skipped": 0}

    content = []

    for case in read_junit(junit_file_paths):
        if case.result is None:
            status = "passed"
            message = None
            summary["passed"] += 1
        elif isinstance(case.result, Skipped):
            status = "skipped"
            summary["skipped"] += 1
            message = case.result.message
        else:
            status = "failed"
            summary["failed"] += 1
            message = case.result.message
        content.append(
            {
                "class": case.classname,
                "name": case.name,
                "status": status,
                "message": message,
            }
        )
    return summary, content


def junit_reporter(junit_file_paths, output_dir_path, report_format="rst"):

    os.makedirs(output_dir_path, exist_ok=True)

    template_path = root_path / "templates" / f"report.{report_format}.tpl"
    summary, content = extract_junit_content(junit_file_paths)

    search_path, template_filename = os.path.split(template_path)

    env = Environment(loader=FileSystemLoader(search_path))

    template = env.get_template(template_filename)

    result = template.render({"junit_content": content})

    # create a pie chart from the summary
    # skip zeros to simplify the chart
    save_figure(
        {k: v for k, v in summary.items() if v != 0}, Path(output_dir_path) / "pie.svg"
    )

    with open(Path(output_dir_path) / f"report.{report_format}", "w") as output_file:
        output_file.write(result)
