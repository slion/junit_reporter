# -*- coding: utf-8 -*-

"""Console script for junit_reporter."""
import sys
from pathlib import Path

from declic import command, argument
from .junit_reporter import junit_reporter


@command(description='Load all the junit files and render the html/rst reports as well as the summary pie chart')
@argument('junit_file_paths', type=str, nargs='+')
@argument('-o', '--output_dir_path', type=str, required=True)
@argument('-f', '--report_format', type=str, choices=['rst', 'html'], default='html')
def main(junit_file_paths, output_dir_path, report_format):
    """Console script for junit_reporter."""
    junit_reporter(junit_file_paths=junit_file_paths, output_dir_path=output_dir_path, report_format=report_format)
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
