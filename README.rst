==============
junit_reporter
==============


A python package that creates HTML and ReStructuredText reports from JUnit files.

* Free software: MIT license

Installation
------------

::

   pip install git+https://gitlab.obspm.fr/slion/junit_reporter.git

Usage
-----

The report generator can be use directly via the command-line interface:

::

    usage: junit_reporter [-h] [-f {rst,html}] -o OUTPUT_DIR_PATH
                junit_file_paths [junit_file_paths ...]

    Load all the junit files and render the html/rst reports as well as the
    summary pie chart

    positional arguments:
    junit_file_paths

    optional arguments:
    -h, --help            show this help message and exit
    -f {rst,html}, --report_format {rst,html}
    -o OUTPUT_DIR_PATH, --output_dir_path OUTPUT_DIR_PATH



Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
