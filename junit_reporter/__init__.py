# -*- coding: utf-8 -*-

"""Top-level package for junit_reporter."""

__author__ = """Sonny Lion"""
__email__ = 'sonny.lion@obspm.fr'
__version__ = '0.1.0'
