Test report
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table:: Test report
   :widths: 30 40 30
   :header-rows: 1

   * - Class
     - Name
     - Status
    {% for row in junit_content %}
   * - {{row['class']}}
     - {{row['name']}}
     - {{row['status']}}
    {% endfor %}
