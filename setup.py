#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

requirements = ['junitparser==1.3.2', 'declic==1.0.2', 'Jinja2==2.10.1', 'matplotlib==3.1.0']

setup_requirements = [ ]

test_requirements = [ ]

setup(
    author="Sonny Lion",
    author_email='sonny.lion@obspm.fr',
    classifiers=[
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="A python package that creates html/rst reports from JUnit files.",
    entry_points={
        'console_scripts': [
            'junit_reporter=junit_reporter.cli:main',
        ],
    },
    install_requires=requirements,
    license="MIT license",
    long_description=readme,
    include_package_data=True,
    keywords='junit_reporter',
    name='junit_reporter',
    packages=find_packages(include=['junit_reporter']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    version='0.1.0',
    zip_safe=False,
)
