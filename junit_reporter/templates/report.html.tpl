<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{test_case_id}}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        body {
            height: 297mm;
            width: 210mm
            margin-left: auto;
            margin-right: auto;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <table class="table table-bordered">
            <thead class="thead-dark">
            <tr>
                <th>Class</th>
                <th>Name</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            {% for row in junit_content %}
            <tr>
                <td>{{row['class']}}</td>
                <td>{{row['name']}}</td>
                <td
                    {% if row['status'] == 'passed' %}
                        class="table-success"
                    {% elif row['status'] == 'failed' %}
                        class="table-danger"
                    {% else %}
                    class="table-warning"
                    {% endif %}

                >{{row['status']}}</td>
            </tr>
            {% if row['message'] %}
            <tr>
                <td/>
                <td colspan="2" class="table-secondary">
                {{row['message']}}
                </td>
            </tr>
            {% endif %}
            {% endfor %}
            </tbody>
        </table>
    </div>
</div>


</body>
</html>
